# Magilo

Magilo is the entry-point GUI of the administration tool suite project [Magilaro](https://gitlab.com/magilaro/magilaro).

## Development

This repository is the implementation of the Magilo GUI tool.

## Requirements

Magilo follow this requirements:
- Magilo is part of the Magilaro tool suite project
- Magilo is the entry-point of the domain specific tool of Magilaro
- Magilo should be available from command-line interface
- Magilo's configuration is based on its plain-text files

## Channel
There is no tool specific channel → let discuss on [#MLO-magilaro](https://web.libera.chat/#MLO-magilaro) on libera-chat.
